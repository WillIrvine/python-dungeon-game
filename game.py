import random
import sys
import sqlite3
from generate_functions import CreateDescription, CreateName
from main_classes import Room, Enemy, Player

helpguide = "Welcome to the dungeon! \n To move a certain direction, simply type that direction (eg: north, south, east, west). \n To fight the monsters you encounter, type 'fight'! \n To loot the room you're in, type 'loot'! \n To drop a bomb, killing a monster instantly, type 'bomb'! \n To see your stats, type 'me'! \n And most importantly, have fun!! Don't get slain! "
# Aww, a cute little help guide!! Who doesn't love that?

def get_results(player, currentRoom):
    with sqlite3.connect("database.db") as con:
        monster_here = False
        currentRoom.leave()
        cur = con.cursor()
        # Checks to see if a room exists at the player's co-ordinates
        cur.execute("SELECT * FROM Room WHERE x_location = ? AND y_location = ?", (player.x_location, player.y_location)) 
        result = cur.fetchone()
        if result == None:
            # Creates room
            currentRoom = Room(player.x_location, player.y_location, CreateDescription(), 0)
        else: # Finds the value of a room
            currentRoom = Room(result[1], result[2], result[3], result[4])
        print(str(currentRoom))
        if random.randint(1,2) == 1:
            enemy = Enemy(CreateName(), int(player.hitpoints / random.randint(1, 4)), 4, False)
            print(str(enemy))
            monster_here = True
            return(player, currentRoom, enemy, monster_here)
        else:
            return(player, currentRoom, monster_here)

def bomb(player, enemy):
    if player.bombs > 0:
        print("Dropped bomb! You killed " + str(enemy.name))
        enemy.die(player)
        player.bombs -= 1
        print("Bombs left: " + str(player.bombs))
        return(player, enemy)
    else:
        print("No bombs available")
        return(player, enemy)

def playGame(): #Game main function
    print(helpguide)
    player = Player(0, 0, 3, 100, 1)
    currentRoom = Room(0, 0, CreateDescription(), False) ## This is where the major changes will have to occur once I implement database
    print(str(currentRoom))
    monster_here = False
    while True:
        choice = input(">>") ## Allows the player to choose their next move.
        ''' 
        This whole next section iterates through the options for player choice.
        Fairly simple, if the player types a certain thing, perform a certain function.
        '''  
        if player.move(choice) != False:
            results = get_results(player, currentRoom)
            if len(results) == 4:
                player = results[0]
                currentRoom = results[1]
                enemy = results[2]
                monster_here = results[3]
            elif len(results) == 3:
                player = results[0]
                currentRoom = results[1]
                monster_here = results[2]
        elif choice.lower() == "fight" and "enemy" in locals() and enemy.dead == False and monster_here == True:
            player.attack(enemy)
        elif choice.lower() == "hitpoints":
            print(player.hitpoints)
        elif choice.lower() == "help":
            print(helpguide)
        elif choice.lower() == "bomb" and "enemy" in locals() and enemy.dead == False and monster_here == True: # Bomb method, simple but it didn't really fit with any object so I included it in the game main code.
            results = bomb(player, enemy)
            player = results[0]
            enemy = results[1]
        elif choice.lower() == "loot":
            currentRoom.loot(player)
        elif choice.lower() == "me":
            print(player)
        else:
            print("No valid move")
            

if __name__ == "__main__":
    with sqlite3.connect("database.db") as con:
        cur = con.cursor()
        cur.execute("DELETE FROM Room")
    playGame()