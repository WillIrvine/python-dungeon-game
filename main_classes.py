import sqlite3
import sys
import random
'''
This script stores the classes for the game file itself, which are: Room, Enemy, and Player
'''
class Room(object): ## Basic room object
    def __init__(self, x_location, y_location, description, looted):
        self.x_location = x_location
        self.y_location = y_location ## Defining the location where the room is. The game operates on a X-Y Grid.
        self.description = description 
        self.looted = looted
    def __repr__(self):
        # Representation method of the Room, this comes in handy in the main game code
        return "You are in a " + str(self.description) +" room at co-ordinates " + str(self.x_location) + ", " + str(self.y_location) + "."
    def loot(self, looter):
        # This function allows players to loot rooms, with a 2 in 3 chance of finding valueable loot.
        if self.looted == 0:
            self.looted = 1
            prizeno = random.randint(1, 3)
            if prizeno == 1:
                # As you can see, looted potions are slightly less effective than dropped potions
                print("You found a healing potion! +10 HP")
                looter.hitpoints += 10
            elif prizeno == 2:
                # Bombs were a recent addition, they allow a "way out" for a particularly dangerous enemy.
                print("You found a bomb! Bombs can be used to instantly kill any enemy.")
                looter.bombs += 1
            elif prizeno == 3:
                print("There's no loot here.")
        else: 
            print("There's no loot here.")
    def leave(self):
        with sqlite3.connect("database.db") as con:
            cur = con.cursor()
            cur.execute("SELECT * FROM Room WHERE x_location = ? AND y_location = ?", (self.x_location, self.y_location)) 
            result = cur.fetchone()
            if result == None:
                cur.execute("INSERT INTO Room (x_location, y_location, description, looted) VALUES (?, ?, ?, ?)", (self.x_location, self.y_location, self.description, self.looted))
        
        
class Player(object): ## Basic player object
    def __init__(self, x_location, y_location, weapon_damage, hitpoints, bombs):
        self.x_location = x_location
        self.y_location = y_location 
        self.weapon_damage = weapon_damage
        self.hitpoints = hitpoints
        self.bombs = bombs
    def __repr__(self): #Represenatation method, allows player to know what their stats are.
        return "You have " + str(self.hitpoints) + " hitpoints! \nYour weapon does " + str(self.weapon_damage) + " damage. \nYou have " + str(self.bombs) + " bombs!"
    def move(self, direction): ## allows player to move through-out the space
        directionsup = {
            "north":1,
            "south":-1
        }
        directionsacross = {
            "east":1,
            "west":-1
        }
        if direction.lower() == "north" or direction.lower() == "south":
            self.y_location += directionsup[direction.lower()]
        elif direction.lower() == "east" or direction.lower() == "west":
            self.x_location += directionsacross[direction.lower()]
        else:
            return False
    def attack(self, target): #The attack method, has a target and inflicts damage on target scaled to the player's weapon damage
        hit_damage = int(self.weapon_damage) * random.randint(0, 4)
        target.hitpoints -= hit_damage
        print("Attacked " + str(target.name) + " for " + str(hit_damage))
        if target.hitpoints < 1:
            print("You killed " + str(target.name)) # Checks to see if you've killed the target
            target.die(self) #Target.die is a method that drops cool loot for the player :)
        else:
            print(str(target.name) + "'s hitpoints: " + str(target.hitpoints)) #The fight continues!
            target.attack(self)
    def die(self):
        print("You died!")
        sys.exit()
    



class Enemy(object): #the basic enemy object
    def __init__(self, name, hitpoints, weapon_damage, dead): 
        self.hitpoints = hitpoints
        self.name = name
        self.weapon_damage = weapon_damage
        self.dead = dead
    def __repr__(self):
        return "A " + str(self.name) + " is here. It has " + str(self.hitpoints) + "hp." #This representation method is what shows when you first encounter a monster
    def die(self, killer): #Upon dying, the creature gives its killer some tasty loot!!
        if self.dead == False: 
            dropitem = random.randint(1, 5)
            self.dead = True
            if dropitem == 1:
                print("You found a healing potion! +25 HP restored")
                killer.hitpoints += 25
                print("HP: " + str(killer.hitpoints))
            elif dropitem == 2 or dropitem == 3:
                print("You got a weapon upgrade!")
                print("Old damage: " + str(killer.weapon_damage))
                killer.weapon_damage += random.randint(1, 3)
                print("New damage: " + str(killer.weapon_damage))
            else:
                print("Oof, the " + self.name + " didn't drop anything :(")
        else:
            print("It's already dead!")
    def attack(self, target): # Enemy's attack method, basically a replica of player method
        hit_damage = int(self.weapon_damage) * random.randint(0, 4)
        target.hitpoints -= hit_damage
        print(str(self.name) + " attacked you for " + str(hit_damage))
        print("Your hitpoints: " + str(target.hitpoints))
        if target.hitpoints < 1:
            target.die()
        else:
            target.attack(self) #this, alongside the player attack method, creates a fight loop for quick and snappy battles 
  