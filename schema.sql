CREATE TABLE Room (
	id integer PRIMARY KEY AUTOINCREMENT,
	x_location integer,
	y_location integer,
	description text,
	looted boolean
);
