import random
'''
This file stores the generating functions for the main file. These are: CreateDescription and CreateName, for Room and Enemy respectively.
'''
def CreateDescription(): #Randomly picks two adjectives to describe the room
    adjectives1 = ["dark", "dimly-lit", "disgusting", "bright", "small", "old", "damp", "scary"]
    adjectives2 = ["mossy", "empty", "well-furnished", "corpse-filled", "messy", "spooky"]
    word1 = adjectives1[random.randint(0, len(adjectives1)-1)]
    word2 = adjectives2[random.randint(0, len(adjectives2)-1)]
    return word1 + ", " + word2

def CreateName(): # Creates names for the monsters you meet, sadly they are no longer David.
    names = ["Leprechaun", "Chimera", "David", "Griffin", "Tiger", "Pirahna Plant", "Headcrab", "Ostrich", "Zombie", "Kraken", "Max", "Nazi", "Koopa", "Terrorist", "Dragon", "Wyvern", "Sea Whip", "Phoenix", "Domestic Cat"]
    return names[random.randint(0, len(names)-1)]